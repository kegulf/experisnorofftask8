**Experis Academy, Norway**

**Authors:**
* **Odd Martin Hansen**

# Task 8: Upgrades

Modify your BMI calculator solution to utilized methods
* [Task 6 - BMI Calculator](https://gitlab.com/kegulf/experisnorofftask6)

Modify your draw square solution to utilize methods
* [Task 3 -  Draw a Square](https://gitlab.com/kegulf/experisnorofftask3)

Modify your nested rectangle solution to utilize methods
* [Task 4 - Nested Rectangles](https://gitlab.com/kegulf/experisnorofftask4)


#### I did all these subtasks when I initially did the respective tasks.